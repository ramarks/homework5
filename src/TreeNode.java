
import java.util.*;

public class TreeNode {

   private String name;
   private TreeNode firstChild;
   private TreeNode nextSibling;

   TreeNode (String n, TreeNode d, TreeNode r) {
	   name = n;
	   firstChild = d;
	   nextSibling = r;
   }
   
   public String getName() {
	   return name;
   }
   
   public TreeNode getFirstChild() {
	   return firstChild;
   }
   
   public TreeNode getNextSibling() {
	   return nextSibling;
   }
   
   public void setFirstChild(TreeNode d) {
	   firstChild = d;
   }
   
   public void setNextSibling(TreeNode r) {
	   nextSibling = r;
   }
   
   public static TreeNode parsePrefix (String s) {
       StringTokenizer st = new StringTokenizer(s, "(,)", true);
       TreeNode tn = null;
       if (st.hasMoreTokens()) {
    	   String token = st.nextToken();
    	   if ( token.equals("(") || token.equals(",") || token.equals(")") )
    		   throw new IllegalArgumentException("Not a proper name for node.");
    	   String nodeName = token.trim();
    	   if (nodeName.contains(" ")) {
    		   throw new IllegalArgumentException("Node name contains space.");
    	   }
    	   tn = new TreeNode(nodeName, null, null);
    	   if (st.hasMoreTokens()) {
    		   String nextToken = st.nextToken();
    		   if (nextToken.equals("(")) {
    			   TreeNode child = parsePrefix(s.substring(token.length() + 1));
    			   tn.setFirstChild(child);
    		   } if (nextToken.equals(",")) {
    			   TreeNode sibling = parsePrefix(s.substring(token.length() + 1));
    			   tn.setNextSibling(sibling);
    		   }
    	   }
       }
	   return tn;  // TODO!!! return the root
   }

   public String rightParentheticRepresentation() {
      StringBuffer b = new StringBuffer();
      TreeNode children;
      
      if ( ( children = getFirstChild() ) != null ) {
    	  b.append( "(" + children.rightParentheticRepresentation() );
    	  
    	  while ( ( children = children.getNextSibling() ) != null ) {
    		  b.append( "," + children.rightParentheticRepresentation() );
    	  }
    	  
    	  b.append( ")" );
      }

      b.append(getName());  
      return b.toString();
   }

   public static void main (String[] param) {
	  String s = "A(B1,C,D)";
      TreeNode t = TreeNode.parsePrefix (s);
      String v = t.rightParentheticRepresentation();
      System.out.println (s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
   }
}

